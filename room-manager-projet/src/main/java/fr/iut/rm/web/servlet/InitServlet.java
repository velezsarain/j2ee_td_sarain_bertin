package fr.iut.rm.web.servlet;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import fr.iut.rm.persistence.dao.RoomDao;
import fr.iut.rm.persistence.domain.Room;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * This servlet simply
 */
@Singleton
public class InitServlet extends HttpServlet {
    /**
     * the dao used to access room persisted data
     */
    @Inject
    RoomDao roomDao;

    /**
     * HTTP GET access
     * @param req use an optional nb parameter to make evidence of transactionnal behavior volontary triggering an exception
     * @param resp response to sent
     * @throws ServletException by container
     * @throws IOException by container
     */
    @Override
    protected void doGet(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
        int quantity = 1;
        String param = req.getParameter("nb");
        if (param != null) {
            quantity = Integer.parseInt(param);
        }
        /**
         * Try to persist new rooms, and do nothing if it fails
         */
        try {
            Room room1 = new Room();
            room1.setName("Server room");
            room1.setCapacity(5);
            room1.setPersonsInside(3);
            roomDao.saveOrUpdate(room1);

            Room room2 = new Room();
            room2.setName("Administrators room");
            room2.setCapacity(10);
            room2.setPersonsInside(2);
            roomDao.saveOrUpdate(room2);

            Room room3 = new Room();
            room3.setName("Coffee room");
            room3.setCapacity(20);
            room3.setPersonsInside(18);
            roomDao.saveOrUpdate(room3);

        } catch (Exception e){

        }
        resp.sendRedirect(resp.encodeRedirectURL("rooms"));
    }
}
