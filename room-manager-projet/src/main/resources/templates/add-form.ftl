<!DOCTYPE html>
<html>
<head>
    <title>Add a new room</title>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/style.css"
    <!-- Bootstrap -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<a href="/room-manager/admin/add-form" onclick="return false">Add rooms</a>
<a href="/room-manager/rooms">List rooms</a>
<a href="/room-manager/admin/home">Admin home</a>
<a href="/room-manager/init">Initialize</a>
<form action="add" method="post">
    Room name: <input type="text" name="name"><br>
    Room capacity: <input type="text" name="capacity"><br>
    <input type="submit" value="Envoyer">
</form>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
</body>
</html>