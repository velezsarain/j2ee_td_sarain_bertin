package fr.iut.rm.web.servlet;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import fr.iut.rm.persistence.dao.RoomDao;
import fr.iut.rm.persistence.domain.Room;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Dumb servlet to output room list
 */
@Singleton
public class DeleteServlet extends HttpServlet {

    @Inject
    RoomDao roomDao;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String roomName = URLDecoder.decode(req.getParameter("name"), "UTF-8");

        Room room = roomDao.findByName(roomName);
        if (room != null){
            roomDao.delete(room);
        }
        resp.sendRedirect(resp.encodeRedirectURL("/room-manager/admin/home"));
    }
}
