/**
 * Provides the web servlets which interact with the view of the application
 */
package fr.iut.rm.web.servlet;