import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.Override;
import java.lang.String;

@WebServlet(name = "qrcode", urlPatterns = "/qrcode")
public class QRCodeServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        QRCodeWriter writer = new QRCodeWriter();
        BitMatrix bitMatrix = null;
        try {
            String url = "http://www.google.com";
            bitMatrix = writer.encode(url, BarcodeFormat.QR_CODE, 300, 300); // on encode l'url en matrice de bits
        }
        catch (WriterException e){
            e.printStackTrace();
        }
        response.setContentType("image/png");
        MatrixToImageWriter.writeToStream(bitMatrix, "png", response.getOutputStream()); // on transforme la matrice en image png
    }
}