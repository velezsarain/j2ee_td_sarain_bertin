package fr.iut.rm.web.servlet;

import com.google.inject.Singleton;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by rembertin on 25/03/15.
 */
@Singleton
public class AddFormServlet extends HttpServlet {
    /**
     * constant for template location
     */
    private static final String ADD_FORM_TEMPLATE = "templates/add-form.ftl";

    /**
     * constant for UTF-8 *
     */
    private static final String TEMPLATE_ENCODING = "UTF-8";
    /**
     * Logger
     */
    private static final Logger logger = LoggerFactory.getLogger(AddFormServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Template freemarkerTemplate = null;

        freemarker.template.Configuration freemarkerConfiguration = new freemarker.template.Configuration();
        freemarkerConfiguration.setClassForTemplateLoading(this.getClass(), "/");
        freemarkerConfiguration.setObjectWrapper(new DefaultObjectWrapper());
        try {
            freemarkerTemplate = freemarkerConfiguration.getTemplate(ADD_FORM_TEMPLATE, TEMPLATE_ENCODING);
        } catch (IOException e) {
            logger.error("Unable to process request, error during freemarker template retrieval.", e);
        }
        Map<String, Object> root = new HashMap<String, Object>();
        PrintWriter out = resp.getWriter();
        assert freemarkerTemplate != null;
        try {
            freemarkerTemplate.process(root, out);
            out.close();
        } catch (TemplateException e) {
            logger.error("Error during template processing", e.getMessage());
        }
        // set mime type
        resp.setContentType("text/html");
    }
}