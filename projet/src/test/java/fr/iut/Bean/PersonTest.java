package fr.iut.Bean;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

/**
 * Created by rembertin on 19/02/15.
 */
public class PersonTest {
    private static Validator validator;

    @BeforeClass
    public static void setUp() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @Test
    public void testIsAValidEmail() {
        Person person = new Person();
        person.setEmail("jeandupont@gmail.com");
        person.setLogin("jdupont");
        Set<ConstraintViolation<Person>> violations = validator.validate(person);
        Assert.assertEquals(0, violations.size());
    }

    @Test
    public void testIsNotAValidEmail() {
        Person person = new Person();
        person.setEmail("jeandupont");
        person.setLogin("jdupont");
        Set<ConstraintViolation<Person>> violations = validator.validate(person);
        Assert.assertEquals(1, violations.size());
    }

    @Test
    public void testIsAValidLogin() {
        Person person = new Person();
        person.setEmail("jean.dupont@gmail.com");
        person.setLogin("jdupont");
        Set<ConstraintViolation<Person>> violations = validator.validate(person);
        Assert.assertEquals(0, violations.size());
    }

    @Test
    public void testIsNotAValidLogin() {
        Person person = new Person();
        person.setEmail("jean.dupont@gmail.com");
        person.setLogin("j");
        Set<ConstraintViolation<Person>> violations = validator.validate(person);
        Assert.assertEquals(1, violations.size());
        person.setLogin("jeeeeaaaaaaannnnn");
        violations = validator.validate(person);
        Assert.assertEquals(1, violations.size());
    }
}
