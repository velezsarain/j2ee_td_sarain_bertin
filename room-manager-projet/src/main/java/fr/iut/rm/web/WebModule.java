package fr.iut.rm.web;

import com.google.inject.persist.PersistFilter;
import com.google.inject.persist.jpa.JpaPersistModule;
import com.google.inject.servlet.ServletModule;
import fr.iut.rm.web.servlet.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by fred on 08/03/15.
 */
public class WebModule extends ServletModule {
    /**
     * Logger
     */
    private static final Logger logger = LoggerFactory.getLogger(WebModule.class);

    @Override
    protected final void configureServlets() {
        super.configureServlets();

        logger.info("WebModule configureServlets started...");
        serve("/").with(InitServlet.class);
        serve("/init").with(InitServlet.class);
        serve("/admin/add").with(AddServlet.class);
        serve("/admin/add-form").with(AddFormServlet.class);
        serve("/rooms").with(ListServlet.class);
        serve("/admin/home").with(AdminServlet.class);
        serve("/rooms/qrcode").with(QRCodeServlet.class);
        serve("/rooms/sign").with(SignServlet.class);
        serve("/delete").with(DeleteServlet.class);

        logger.info(" install JpaPersistModule room-manager");
        install(new JpaPersistModule("room-manager"));

        logger.info("   install servlet filter");
        filter("/*").through(PersistFilter.class);

        logger.info("WebModule configureServlets ended.");
    }
}
