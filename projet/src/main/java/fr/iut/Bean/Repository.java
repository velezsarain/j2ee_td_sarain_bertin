package fr.iut.Bean;

import org.hibernate.validator.constraints.URL;

/**
 * Created by shesarain on 13/02/15.
 */
public class Repository {
    @URL
    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
