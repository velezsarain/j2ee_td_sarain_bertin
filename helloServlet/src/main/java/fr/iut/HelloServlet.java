package fr.iut;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by rembertin on 04/03/15.
 */
public class HelloServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter() ;
        out.println( "<HTML>" ) ;
        out.println( "<HEAD>" ) ;
        out.println( "<TITLE>Création de la première servlet</TITLE>" ) ;
        out.println( "</HEAD>" ) ;
        out.println( "<BODY>" ) ;
        out.println( "<H1>Hello world !</H1>" ) ;
        out.println("<P>Bienvenue sur notre première page générée par un servlet !</P>");
        out.println( "</BODY>" ) ;
        out.println( "</HTML>" ) ;
        out.close() ;
    }
}
