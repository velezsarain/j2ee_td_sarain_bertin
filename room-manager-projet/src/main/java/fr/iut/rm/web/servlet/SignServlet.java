package fr.iut.rm.web.servlet;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import fr.iut.rm.persistence.dao.RoomDao;
import fr.iut.rm.persistence.domain.Room;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLDecoder;

/**
 * Created by rembertin on 24/03/15.
 */
@Singleton
public class SignServlet extends HttpServlet {
    public final static String ENTER_PARAM = "enter";
    public final static String LEAVE_PARAM = "leave";

    @Inject
    RoomDao roomDao;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String type = req.getParameter("type");
        String roomName = URLDecoder.decode(req.getParameter("name"), "UTF-8");

        Room room = roomDao.findByName(roomName);
        if (room != null){
            if (type.equals(ENTER_PARAM)) {
                if (room.getPersonsInside() < room.getCapacity())
                    room.setPersonsInside(room.getPersonsInside()+1);
            }
            else if (type.equals(LEAVE_PARAM)){
                if (room.getPersonsInside() > 0)
                    room.setPersonsInside(room.getPersonsInside()-1);
            }
            roomDao.saveOrUpdate(room);
        }
        resp.sendRedirect(resp.encodeRedirectURL("../rooms"));
    }
}
