<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>${title}</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/style.css">
  </head>

  <body>
    <h1>${result}</h1>
    <a href="/room-manager/main.html">Main page</a>
    <a href="/room-manager/add-form">Add rooms</a>
    <a href="/room-manager/rooms">List rooms</a>
    <a href="/room-manager/admin/home">Admin home</a>
    <a href="/room-manager/init">Initialize</a>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>