package fr.iut.rm.dao;

import fr.iut.rm.dao.guice.AbstractDaoTest;
import fr.iut.rm.persistence.dao.RoomDao;
import fr.iut.rm.persistence.domain.Room;
import org.junit.Test;

import java.util.List;

import static org.fest.assertions.Assertions.assertThat;

public class RoomDaoTest extends AbstractDaoTest {

    @Test
    public void testPersistAndFindByName() {
        Room room = new Room();
        room.setName("Test Room");

        RoomDao dao = this.getRoomDao();
        dao.saveOrUpdate(room);
        Room saved = dao.findByName(room.getName());

        assertThat(saved).isNotNull();
        assertThat(saved.getName()).isNotNull();
        assertThat(saved.getName()).isEqualTo(room.getName());
    }

    @Test
    public void testFindAll(){
        Room room1 = new Room();
        room1.setName("Test Room 1");
        Room room2 = new Room();
        room2.setName("Test Room 2");

        RoomDao dao = this.getRoomDao();
        dao.saveOrUpdate(room1);
        dao.saveOrUpdate(room2);

        List<Room> savedRooms = dao.findAll();

        assertThat(savedRooms.size()).isEqualTo(2);
        assertThat(savedRooms.contains(room1)).isTrue();
        assertThat(savedRooms.contains(room2)).isTrue();
    }

    @Test
    public void testRemove(){
        Room room = new Room();
        room.setName("Test Room");

        RoomDao dao = this.getRoomDao();
        dao.saveOrUpdate(room);
        List<Room> saved = dao.findAll();
        assertThat(saved.size()).isEqualTo(1);
        dao.delete(room);
        saved = dao.findAll();
        assertThat(saved.size()).isEqualTo(0);
    }

    private RoomDao getRoomDao() {
        return this.injector.getInstance(RoomDao.class);
    }
}
