package fr.iut.rm.web.servlet;

import com.google.inject.Singleton;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.Override;
import java.lang.String;
import java.net.URLEncoder;

@Singleton
public class QRCodeServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String roomName = request.getParameter("name");
        String queryType = request.getParameter("type");

        String url = request.getRequestURL().toString() +  "/../sign?type=" + queryType + "&name=" + URLEncoder.encode(roomName, "UTF-8");
        QRCodeWriter writer = new QRCodeWriter();
        BitMatrix bitMatrix = null;
        try {
            bitMatrix = writer.encode(url, BarcodeFormat.QR_CODE, 300, 300); // encoding URL into a QRCode matrix
        }
        catch (WriterException e){
            e.printStackTrace();
        }
        response.setContentType("image/png");
        MatrixToImageWriter.writeToStream(bitMatrix, "png", response.getOutputStream()); //putting matrix into image
    }
}