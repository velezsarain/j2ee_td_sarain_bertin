package fr.iut.Bean;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;


/**
 * Created by rembertin on 19/02/15.
 */
public class RepositoryTest {
    private static Validator validator;

    @BeforeClass
    public static void setUp() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }
    @Test
    public void testIsAValidURL(){
        Repository repository = new Repository();
        repository.setUrl("http://www.google.com");
        Set<ConstraintViolation<Repository>> violations = validator.validate(repository);
        Assert.assertEquals(0, violations.size());
    }

    @Test
    public void testIsNotAValidURL(){
        Repository repository = new Repository();
        repository.setUrl("coucou");
        Set<ConstraintViolation<Repository>> violations = validator.validate(repository);
        Assert.assertEquals(1, violations.size());
    }

}
