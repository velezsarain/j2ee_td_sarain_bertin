package fr.iut.Bean;

import fr.iut.validation.Login;
import org.hibernate.validator.constraints.Email;

/**
 * Created by shesarain on 13/02/15.
 */
public class Person {
    @Email
    private String email;

    private String firstName;
    private String lastName;
    @Login
    private String login;
    private boolean isStudent;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public boolean isStudent() {
        return isStudent;
    }

    public void setStudent(boolean isStudent) {
        this.isStudent = isStudent;
    }
}
