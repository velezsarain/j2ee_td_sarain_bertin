package fr.iut.Bean;

import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.Set;

/**
 * Created by shesarain on 13/02/15.
 */
public class Projet {
    Collection<Document> documents;
    Repository repository;
    Set<Person> customers;
    Set<Person> students;
    Set<Person> teachers;
    String description;
    @NotNull
    String name;
}
