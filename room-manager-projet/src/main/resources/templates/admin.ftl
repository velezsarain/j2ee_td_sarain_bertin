<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>All the rooms</title>

    <!-- Bootstrap -->

    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../css/style.css">
</head>
<body>
    <a href="/room-manager/admin/add-form">Add rooms</a>
    <a href="/room-manager/rooms" >List rooms</a>
    <a href="/room-manager/admin/home" onclick="return false">Admin home</a>
    <a href="/room-manager/init">Initialize</a>
    <table>
        <tr>
            <th>Name</th>
            <th>Capacity</th>
            <th>Free places</th>
        </tr>
        <#list rooms as room>
        <tr>
            <#assign occupation = room.personsInside/room.capacity*100>
            <td>${room.name}</td>
            <td>${room.capacity}</td>
            <#if occupation < 40>
                <td class="greenBg">${room.capacity - room.personsInside}</td>
            <#elseif occupation < 70>
                <td class="orangeBg">${room.capacity - room.personsInside}</td>
            <#elseif occupation gte 70>
                <td class="redBg">${room.capacity - room.personsInside}</td>
            </#if>
            <td>
                <a href="../rooms/sign?name=${room.name}&type=enter">Enter room</a><br/>
                <img src="../rooms/qrcode?name=${room.name}&type=enter" alt="QR Code to enter the room">
            </td>
            <td>
                <a href="../rooms/sign?name=${room.name}&type=leave">Leave room</a><br/>
                <img src="../rooms/qrcode?name=${room.name}&type=leave" alt="QR Code to leave the room">
            </td>
            <td>
                <a href="../delete?name=${room.name}">Delete room</a><br/>
             </td>
        </tr>
    </#list>
    </table>
            <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery.min.js"></script>
            <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
</body>
</html>
