package fr.iut.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Created by rembertin on 17/02/15.
 */
public class LoginValidator implements ConstraintValidator<Login, String> {

    @java.lang.Override
    public void initialize(Login login) {    }

    @java.lang.Override
    public boolean isValid(String value, ConstraintValidatorContext constraintValidatorContext) {
        return (value.length() >= 2) && (value.length() <= 8);
    }
}
