package fr.iut.rm.web.servlet;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import fr.iut.rm.persistence.dao.RoomDao;
import fr.iut.rm.persistence.domain.Room;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Dumb servlet to output room list
 */
@Singleton
public class ListServlet extends HttpServlet {
    /**
     * constant for template location
     */
    private static final String LIST_TEMPLATE = "templates/list.ftl";

    /**
     * constant for UTF-8 *
     */
    private static final String TEMPLATE_ENCODING = "UTF-8";
    /**
     * Logger
     */
    private static final Logger logger = LoggerFactory.getLogger(ListServlet.class);
    /**
     * the dao to access rooms stored in DB *
     */
    @Inject
    RoomDao roomDao;

    /**
     * GET access
     * @param request nothing specified
     * @param response nothing specified, must be HTML format
     * @throws ServletException by container
     * @throws IOException by container
     */
    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        Template freemarkerTemplate = null;

        freemarker.template.Configuration freemarkerConfiguration = new freemarker.template.Configuration();
        freemarkerConfiguration.setClassForTemplateLoading(this.getClass(), "/");
        freemarkerConfiguration.setObjectWrapper(new DefaultObjectWrapper());
        try {
            freemarkerTemplate = freemarkerConfiguration.getTemplate(LIST_TEMPLATE, TEMPLATE_ENCODING);
        } catch (IOException e) {
            logger.error("Unable to process request, error during freemarker template retrieval.", e);
        }

        Map<String, Object> root = new HashMap<String, Object>();
        // navigation data and links
        List<Room> rooms = roomDao.findAll();
        //response.getOutputStream().println("Rooms are :");
        root.put("rooms", rooms);
        /**
         * Try to process what has been done
         */
        PrintWriter out = response.getWriter();
        assert freemarkerTemplate != null;
        try {
            freemarkerTemplate.process(root, out);
            out.close();
        } catch (TemplateException e) {
            logger.error("Error during template processing", e.getMessage());
        }

        // set mime type
        response.setContentType("text/html");
    }
}
