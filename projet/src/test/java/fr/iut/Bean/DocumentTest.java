package fr.iut.Bean;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

import java.util.Date;


public class DocumentTest {

    private static Validator validator;

    @BeforeClass
    public static void setUp() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @Test
    public void testIsAValidCreationDate(){
        Document document = new Document();
        document.setCreationDate(new Date(System.currentTimeMillis() - 5000 ));
        Set<ConstraintViolation<Document>> violations = validator.validate(document);
        Assert.assertEquals(4, violations.size());
    }

    @Test
    public void testIsNotAValidCreationDate(){
        Document document = new Document();
        document.setCreationDate(new Date(System.currentTimeMillis() + 5000 ));
        Set<ConstraintViolation<Document>> violations = validator.validate(document);
        Assert.assertEquals(5, violations.size());
    }

    @Test
    public void testIsAValidLastModificationDate(){
        Document document = new Document();
        document.setLastModificationDate(new Date(System.currentTimeMillis() - 5000));
        Set<ConstraintViolation<Document>> violations = validator.validate(document);
        Assert.assertEquals(3, violations.size());
    }

    @Test
    public void testIsNotAValidLastModificationDate(){
        Document document = new Document();
        document.setLastModificationDate(new Date(System.currentTimeMillis() + 5000 ));
        Set<ConstraintViolation<Document>> violations = validator.validate(document);
        Assert.assertEquals(4, violations.size());
    }

    @Test
    public void testIsAValidCreator(){
        Person person = new Person();
        person.setFirstName("Jean");
        person.setLogin("francis");
        Document document = new Document();
        document.setCreator(person);
        Set<ConstraintViolation<Document>> violations = validator.validate(document);
        Assert.assertEquals(3, violations.size());
    }

    @Test
    public void testIsNotAValidCreator(){
        Document document = new Document();
        Set<ConstraintViolation<Document>> violations = validator.validate(document);
        Assert.assertEquals(4, violations.size());
    }

    @Test
    public void testIsAValidLastModifier(){
        Person person = new Person();
        person.setFirstName("Jean");
        person.setLogin("francis");
        Document document = new Document();
        document.setLastModifier(person);
        Set<ConstraintViolation<Document>> violations = validator.validate(document);
        Assert.assertEquals(3, violations.size());
    }

    @Test
    public void testIsNotAValidLastModifier(){
        Document document = new Document();
        Set<ConstraintViolation<Document>> violations = validator.validate(document);
        Assert.assertEquals(4, violations.size());
    }

    @Test
    public void testIsAValidTitle(){
        Document document = new Document();
        document.setTitle("titre");
        Set<ConstraintViolation<Document>> violations = validator.validate(document);
        Assert.assertEquals(3, violations.size());
    }

    @Test
    public void testIsNotAValidTitle(){
        Document document = new Document();
        Set<ConstraintViolation<Document>> violations = validator.validate(document);
        Assert.assertEquals(4, violations.size());
    }
}
