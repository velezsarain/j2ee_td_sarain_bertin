package fr.iut.rm.persistence.domain;

import javax.persistence.*;
import javax.validation.constraints.Min;

/**
 * A classic room
 */
@Entity
@Table(name = "room")
public class Room {
    /**
     * sequence generated id
     */
    @Id
    @Column(name = "room_name")
    @GeneratedValue
    private long id;

    /**
     * Room's capacity
     */
    @Column(nullable = false, unique = false)
    @Min(1)
    private int capacity;

    /**
     * Room's name
     */
    @Column(nullable = false, unique = true)
    private String name;



    /**
     * Number of persons in the room
     */
    @Column(nullable = true, unique = false)
    private int personsInside;

    /**
     * Default constructor (initialize a room with default values)
     */
    public Room() {
        name = "Room n°"+id;
        capacity = 1;
        personsInside = 0;
    }

    /**
     * anemic getter
     *
     * @return the room's id
     */
    public long getId() {
        return id;
    }

    /**
     * anemic setter
     *
     * @param id the new id
     */
    public void setId(final long id) {
        this.id = id;
    }

    /**
     * anemic getter
     *
     * @return the calling number
     */
    public String getName() {
        return name;
    }

    /**
     * anemic setter
     *
     * @param name the new calling number to set
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * anemic getter
     *
     * @return the room's capacity
     */
    public int getCapacity() {
        return capacity;
    }

    /**
     * anemic getter
     * @param capacity the new room's capacity
     */
    public void setCapacity(final int capacity) {
        this.capacity = capacity;
    }

    /**
     * anemic getter
     * @return the number of persons inside
     */
    public int getPersonsInside() {
        return personsInside;
    }

    /**
     * anemic setter
     * @param personsInside the new number of persons inside
     */
    public void setPersonsInside(final int personsInside) {
        this.personsInside = personsInside;
    }
}
